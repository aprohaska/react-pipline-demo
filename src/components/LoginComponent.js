import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {Button, Form, FormGroup, Label, Input}
from 'reactstrap';
import {useNavigate} from 'react-router-dom';

const LoginPage = () => {
    const navigate = useNavigate();

    return(

        <Form className="login-form">
        <h2 className='text-center'>Welcome</h2>
        <FormGroup>
            <Label>User:</Label>
            <Input type="username" placeholder='UserName'/>  
        </FormGroup>
        <FormGroup>
            <Label>Password:</Label>
            <Input type="password" placeholder='Password'/>  
        </FormGroup>
        <div className='text-center'>
        <Button className='btn-dark btn-block' onClick={() => navigate("/home")}>Login</Button>
        <span className="p-2"></span>
        <Button className='btn-dark btn-block' onClick={() => navigate("/")}>Cancel</Button>
        </div>
        <div className='text-center'>
            <a href='/sign-up'>Sign up</a>
            <span className="p-2">|</span>
            <a href='/sign-up'>Forgot Password</a>
        </div>
        </Form>
  
   
      

    );
}

export default LoginPage;
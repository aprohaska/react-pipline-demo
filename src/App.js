import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes } from 'react-router-dom';
import ListEmployeeComponent from './components/ListEmployeeComponent';  
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import CreateEmployeeComponent from './components/CreateEmployeeComponent';
import ViewEmployeeComponent from './components/ViewEmployeeComponent';
import LoginPage from './components/LoginComponent';


function App() {
  return (
    <div>
      <Router>
        <HeaderComponent/>
        <div className="container">
          <Routes>
            <Route path="/home" element = {<ListEmployeeComponent/>}/>
            <Route path="/employees" element = {<ListEmployeeComponent/>}/>
            <Route path="/add-employee/:id" element = {<CreateEmployeeComponent/>}/>
            <Route path="/view-employee/:id" element = {<ViewEmployeeComponent/>}/>
            <Route path="/" element = {<LoginPage/>}/>
            {/* <Route path="/add-employee/_add" element = {<CreateEmployeeComponent/>}/> */}
            {/* <Route path="/update-employee:id" element = {UpdateEmployeeComponent}></Route> */}
          </Routes>
        </div>
        <FooterComponent/>
      </Router>
    </div>
     


  );
}

export default App;
FROM node
WORKDIR /home/node/react-frontend
COPY package.json .
RUN npm install
COPY . . 
EXPOSE 3000
CMD ["npm", "start"]